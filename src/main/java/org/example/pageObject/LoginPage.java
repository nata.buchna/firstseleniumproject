package org.example.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends BasePage{

    public LoginPage(WebDriver driver) {
        super(driver);
    }
    public static final String LOGIN_PAGE = "https://the-internet.herokuapp.com/login";
    public final String SECURE_PAGE = "https://the-internet.herokuapp.com/secure";

    public final String LOGGED = "You logged into a secure area!";
    public final String NOT_LOGGED = "You must login to view the secure area!";

    WebElement userName =  driver.findElement(By.id("username"));
    WebElement password =  driver.findElement(By.id("password"));

    public WebElement button = driver.findElement(By.cssSelector("button"));

    public void setUserName(){
        userName.sendKeys("tomsmith");
    }

    public void setPassword(){
        password.sendKeys("SuperSecretPassword!");
    }


    public String getText() {
        WebElement element = driver.findElement(By.id("flash"));
        String text = element.findElement(By.tagName("a")).getText();
        return element.getText().replace(text, "").trim();
    }

}
