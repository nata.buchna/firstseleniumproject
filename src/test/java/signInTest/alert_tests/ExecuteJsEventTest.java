package signInTest.alert_tests;

import org.example.driverManager.DriverManager;
import org.example.pageObject.AlertPage;
import org.example.pageObject.MainPage;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import signInTest.baseTest.BaseTest;

import static org.example.enums.AlertButtons.*;
import static org.example.pageObject.AlertPage.*;
import static org.example.pageObject.MainPage.MAIN_URL;

public class ExecuteJsEventTest extends BaseTest {
    MainPage mainPage;
    AlertPage alertsPage;

    @BeforeClass
    public void setUp() {
        openUrl(MAIN_URL);
        mainPage = new MainPage(DriverManager.getDriver());
        alertsPage = new AlertPage(DriverManager.getDriver());
    }

    @Test
    public void alertTest() {
        mainPage.scrollToFooter(mainPage.btnForLink("javascript_alerts"));
        mainPage.clickOnLink("javascript_alerts");
        alertsPage.executeJSEvent(ALERT);
        Assert.assertEquals(alertsPage.switchToAlertGetText(true), ALERT_TEXT);
        Assert.assertEquals(alertsPage.getResultText(), "You successfully clicked an alert");
    }

    @Test
    public void confirmDismissTest() {
        mainPage.scrollToFooter(mainPage.btnForLink("javascript_alerts"));
        mainPage.clickOnLink("javascript_alerts");
        alertsPage.executeJSEvent(CONFIRM);
        Assert.assertEquals(alertsPage.switchToAlertGetText(false), CONFIRM_TEXT);
        Assert.assertEquals(alertsPage.getResultText(), "You clicked: Cancel");
    }

    @Test
    public void confirmAcceptTest() {
        mainPage.scrollToFooter(mainPage.btnForLink("javascript_alerts"));
        mainPage.clickOnLink("javascript_alerts");
        alertsPage.executeJSEvent(CONFIRM);
        Assert.assertEquals(alertsPage.switchToAlertGetText(true), CONFIRM_TEXT);
        Assert.assertEquals(alertsPage.getResultText(), "You clicked: Ok");
    }

    @DataProvider(name = "TestParameters")
    Object[][] dataParameters() {
        return new Object[][]{
                {true, "Some text", "You entered: Some text"},
                {false, "Some text", "You entered: null"},
                {true, "", "You entered:"},
                {false, "", "You entered: null"}
        };
    }

    @Test(dataProvider = "TestParameters")
    public void promptTest(boolean confirm, String enteredText, String expectedText) {
        mainPage.scrollToFooter(mainPage.btnForLink("javascript_alerts"));
        mainPage.clickOnLink("javascript_alerts");
        alertsPage.executeJSEvent(PROMPT);
        Assert.assertEquals(alertsPage.switchToAlertGetText(confirm, enteredText), PROMPT_TEXT);
        Assert.assertEquals(alertsPage.getResultText(), expectedText);
    }

    @AfterMethod
    public void backDriver() {
        DriverManager.getDriver().navigate().back();
    }
}
