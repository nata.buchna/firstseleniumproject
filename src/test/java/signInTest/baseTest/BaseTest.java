package signInTest.baseTest;

import org.example.driverManager.DriverManager;
import org.testng.annotations.AfterTest;

public class BaseTest {


    public void openUrl(String expectedURL) {
        DriverManager.getDriver().get(expectedURL);
    }

    @AfterTest
    public void closeDriver() {
        DriverManager.quitDriver();
    }

    public String getCurrentUrl() {
        return DriverManager.getDriver().getCurrentUrl();
    }


}
