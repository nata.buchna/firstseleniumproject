package signInTest.loginCashTest;

import org.example.driverManager.DriverManager;
import org.example.pageObject.LoginPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import signInTest.baseTest.BaseTest;

import static org.example.driverManager.DriverManager.driver;
import static org.example.driverManager.DriverManager.getDriver;
import static org.example.pageObject.LoginPage.LOGIN_PAGE;

public class LoginCookies extends BaseTest {
    LoginPage loginPage;

    @BeforeClass
    public void setUp() {
        openUrl(LOGIN_PAGE);
        loginPage = new LoginPage(DriverManager.getDriver());
    }

    @Test
    public void login() {
        Assert.assertEquals(getDriver().getCurrentUrl(), LOGIN_PAGE);
        loginPage.setUserName();
        loginPage.setPassword();
        loginPage.button.click();
        Assert.assertEquals(getCurrentUrl(), loginPage.SECURE_PAGE);
        Assert.assertEquals(loginPage.getText(), loginPage.LOGGED);

        driver.manage().deleteAllCookies();
        driver.navigate().refresh();
        Assert.assertEquals(loginPage.getText(), loginPage.NOT_LOGGED);
    }

}
